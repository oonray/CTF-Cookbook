from stegano import lsbset
from stegano.lsbset import generators
import inspect

all_generators = inspect.getmembers(generators, inspect.isfunction)
length = len(all_generators)
left = length
for i in all_generators:
    print("Starting generator nr {}, there is {} generators left --------------------------------\n".format(length-left,left))
    try:
        msg = lsbset.reveal("./cat_with_secrets.png",i[1]())
        print(msg)
    except Exception as e:print("error: ",e)
    left = left-1

exit()