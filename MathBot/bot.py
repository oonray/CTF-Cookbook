from socket import socket,AF_INET,SOCK_STREAM
import numpy as np

nc_str= "nc 195.154.53.62 1337"


math = {
    "+": lambda x,y: int(x) + int(y),
    "%": lambda x,y: int(x) % int(y),
    "-": lambda x,y: int(x) - int(y),
    "*": lambda x,y: int(x) * int(y),
    "/": lambda x,y: np.divide(float(x),float(y))
}

def calc(a):
    num1,opperator,num2,equals = a.split()
    return math[opperator](num1,num2)

s = socket(AF_INET,SOCK_STREAM)
s.connect((nc_str.split()[1],int(nc_str.split()[2])))
while s:
    a = s.recv(1024).decode()
    print(a)
    a = [i for i in a.split("\n") if "=" in i]
    a = a[0]
    x = str(int(calc(a)))
    print(x)
    s.send(x.encode())
    s.send("\n".encode())




